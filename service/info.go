package service

import (
	"context"

	"bitbucket.org/pixeon/go-rest-sample/domain"
)

//Info is the implementation of InfoService
//According to go standard, since we are under service package
//we do not need the suffix service cause it will be used as service.Info
type Info struct {
	storage domain.InfoStorage
}

//NewInfo is the Info constructor. It will retrun a pointer to Info
func NewInfo(storage domain.InfoStorage) *Info {
	return &Info{
		storage: storage,
	}
}

func (i *Info) Get(ctx context.Context) (*domain.Info, error) {
	return i.storage.Get(ctx)
}
