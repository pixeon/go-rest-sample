package env_test

import (
	"os"
	"strconv"
	"testing"

	"bitbucket.org/pixeon/go-rest-sample/env"
)

func TestGetEnvRequired(t *testing.T) {
	key := "foo"
	want := "bar"

	err := os.Setenv(key, want)
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	got := env.GetEnvRequired(key)
	if got != want {
		t.Errorf("unexpected environment variable value: got %v want %v", got, want)
	}
}

func TestGetenvIntRequired(t *testing.T) {
	key := "foo"
	want := 1

	err := os.Setenv(key, strconv.Itoa(want))
	if err != nil {
		t.Errorf("unexpected error: %v", err)
	}

	got := env.GetEnvIntRequired(key)
	if got != want {
		t.Errorf("unexpected environment variable value: got %v want %v", got, want)
	}
}
