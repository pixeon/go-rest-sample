package env

import (
	"log"
	"os"
	"strconv"
)

const (
	requiredMessage   = "missing required environment variable"
	invalidIntMessage = "invalid int environment variable: %s(%+v)"
)

// GetEnvRequired checks if key is set and returns it.
// Log messange and exit, otherwise.
func GetEnvRequired(key string) string {
	e := os.Getenv(key)
	if e == "" {
		log.Fatal(requiredMessage)
	}

	return e
}

// GetEnvIntRequired checks if key is set and returns it.
// Log messange and exit, otherwise.
func GetEnvIntRequired(key string) int {
	e := GetEnvRequired(key)
	i, err := strconv.Atoi(e)
	if err != nil {
		log.Fatal(invalidIntMessage)
	}

	return i
}
