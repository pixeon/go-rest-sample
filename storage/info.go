package storage

import (
	"context"
	"log"

	"bitbucket.org/pixeon/go-rest-sample/domain"
)

type Info struct {
}

func NewInfo() *Info {
	return &Info{}
}

func (i *Info) Get(ctx context.Context) (*domain.Info, error) {
	log.Println("Creating a dummy info for a while")
	return &domain.Info{
		Name:    "GO-REST-SAMPLE",
		Version: "1.0.0-alpha",
	}, nil
}
