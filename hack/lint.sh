#!/bin/sh

set -e

gometalinter --tests --vendor --deadline 5m ./...
