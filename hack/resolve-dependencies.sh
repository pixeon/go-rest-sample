#!/bin/sh

set -e

dep ensure -v
chown -R $USER_ID:$GROUP_ID ./vendor
