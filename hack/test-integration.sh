#!/bin/sh

set -e

go test -v $(go list ./...) -tags=integration
