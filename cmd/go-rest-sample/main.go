package main

import (
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/pixeon/go-rest-sample/domain"
	"bitbucket.org/pixeon/go-rest-sample/env"
	"bitbucket.org/pixeon/go-rest-sample/server"
	"bitbucket.org/pixeon/go-rest-sample/service"
	"bitbucket.org/pixeon/go-rest-sample/storage"
)

const (
	envPort string = "PORT"
)

func main() {
	port := env.GetEnvRequired(envPort)
	server := server.New(port, getInfoService())
	server.Start()

	stopChan := make(chan os.Signal, 1)
	signal.Notify(stopChan, syscall.SIGTERM, syscall.SIGINT)
	<-stopChan
	server.Shutdown()
}

func getInfoService() domain.InfoService {
	return service.NewInfo(getInfoStorage())
}

func getInfoStorage() domain.InfoStorage {
	return storage.NewInfo()
}
