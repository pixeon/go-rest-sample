package domain

import "context"

//Info is the resource t get app information
type Info struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

//InfoService should be used to handle with Info resource
type InfoService interface {
	Get(ctx context.Context) (*Info, error)
}

//InfoStorage should be used when you are interact to info database
type InfoStorage interface {
	Get(ctx context.Context) (*Info, error)
}
