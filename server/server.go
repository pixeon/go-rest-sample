package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/pixeon/go-rest-sample/domain"
	"bitbucket.org/pixeon/go-rest-sample/server/handler"
)

//Server encapsulates a http.Server
type Server struct {
	server *http.Server
}

//New is the constructor of type Server
func New(
	port string,
	info domain.InfoService,
) *Server {
	return &Server{
		server: &http.Server{
			Addr:         fmt.Sprintf(":%s", port),
			Handler:      handler.New(info),
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 55 * time.Second,
		},
	}
}

//Start calls http.server.ListenAndServe
func (s *Server) Start() {
	go func() {
		if err := s.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalln("Impossible to start the server")
		}
	}()
}

//Shutdown calls http.server.Shutdown
func (s *Server) Shutdown() {
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()
	if err := s.server.Shutdown(ctx); err != nil && err != http.ErrServerClosed {
		return
	}
}
