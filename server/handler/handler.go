package handler

import (
	"log"
	"net/http"

	"bitbucket.org/pixeon/go-rest-sample/domain"
	"github.com/gin-gonic/gin"
)

//New creates a gin Engine which is already set with routes
func New(
	infoService domain.InfoService,
) *gin.Engine {
	gin.SetMode(gin.ReleaseMode)

	router := gin.New()

	router.GET("/info", GetInfoHandler(infoService))

	return router
}

func recovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				log.Println("request failed")
				c.AbortWithStatus(http.StatusInternalServerError)
			}
		}()
		c.Next()
	}
}
