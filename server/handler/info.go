package handler

import (
	"log"
	"net/http"

	"bitbucket.org/pixeon/go-rest-sample/domain"
	"github.com/gin-gonic/gin"
)

//Get is the handler to retrieve Info resourcess
func GetInfoHandler(serv domain.InfoService) func(c *gin.Context) {
	return func(c *gin.Context) {
		inf, err := serv.Get(c)
		if err != nil {
			log.Println("")
			return
		}
		c.JSON(http.StatusOK, inf)
	}
}
